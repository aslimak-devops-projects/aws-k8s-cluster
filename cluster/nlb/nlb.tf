# Create an AWS Network Load Balancer
resource "aws_lb" "network" {
  name = "${var.cluster_name}-nlb"

  internal           = false
  load_balancer_type = "network"
  subnets            = var.subnet_ids
  security_groups    = [var.security_group_id]
}

# HTTP
# Create a http listener
resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.network.arn
  protocol          = "TCP"
  port              = 80

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.http.arn
  }
}

# Create a http target group
resource "aws_lb_target_group" "http" {
  name = "${var.cluster_name}-http-nlb-tg"

  vpc_id   = var.vpc_id
  protocol = "TCP"
  port     = 30080

  target_type = "instance"

  health_check {
    enabled = true

    protocol = "HTTP"
    path     = "/healthz"
  }
}

# HTTPS
# Create a https listener
resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.network.arn
  protocol          = "TCP"
  port              = 443

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.https.arn
  }
}

# Create a https target group
resource "aws_lb_target_group" "https" {
  name = "${var.cluster_name}-https-nlb-tg"

  vpc_id   = var.vpc_id
  protocol = "TCP"
  port     = 30443

  target_type = "instance"

  health_check {
    enabled = true

    protocol = "HTTP"
    path     = "/healthz"
  }
}
