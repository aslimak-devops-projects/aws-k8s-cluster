resource "aws_autoscaling_attachment" "http_target_group" {
  autoscaling_group_name = var.workers_asg_id
  lb_target_group_arn    = aws_lb_target_group.http.arn
}

resource "aws_autoscaling_attachment" "https_target_group" {
  autoscaling_group_name = var.workers_asg_id
  lb_target_group_arn    = aws_lb_target_group.https.arn
}
