variable "vpc_id" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "security_group_id" {
  type = string
}

variable "workers_asg_id"{
  type = string
}

variable "cluster_name" {
  type    = string
  default = "k8s-cluster"
}
