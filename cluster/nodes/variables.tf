variable "subnet_ids" {
  type = list(string)
}

variable "security_group_id" {
  type = string
}

variable "key_name" {
  type = string
}

variable "cluster_name" {
  type    = string
  default = "k8s-cluster"
}