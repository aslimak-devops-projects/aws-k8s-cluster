#!/bin/bash

sudo apt update
sudo apt install awscli -y

export INSTALL_RKE2_VERSION="v1.29.1+rke2r1"
export INSTALL_RKE2_CHANNEL="latest"

curl -sfL https://get.rke2.io | sudo sh -

sudo mkdir -p /etc/rancher/rke2
sudo echo "disable: rke2-ingress-nginx" > /etc/rancher/rke2/config.yaml

sudo systemctl enable rke2-server.service
sudo systemctl start rke2-server.service

TOKEN="$(curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600")"
HOSTNAME="$(curl http://169.254.169.254/latest/meta-data/local-ipv4 -H "X-aws-ec2-metadata-token: $TOKEN")"
TOKEN="$(sudo cat /var/lib/rancher/rke2/server/node-token)"

echo "server: https://$HOSTNAME:9345" > /home/ubuntu/rke-config.yaml
echo "token: $TOKEN" >> /home/ubuntu/rke-config.yaml
chown ubuntu /home/ubuntu/rke-config.yaml

aws s3 cp /home/ubuntu/rke-config.yaml s3://devops-project-bucket

sudo snap install kubectl --classic
sudo snap install helm --classic

mkdir /home/ubuntu/.kube
sudo cp /etc/rancher/rke2/rke2.yaml /home/ubuntu/.kube/config
chown ubuntu /home/ubuntu/.kube
chown ubuntu /home/ubuntu/.kube/config

export KUBECONFIG=/home/ubuntu/.kube/config

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add jetstack https://charts.jetstack.io
helm repo update

kubectl create namespace ingress-nginx

helm upgrade -i ingress-nginx ingress-nginx/ingress-nginx \
    --namespace ingress-nginx \
    --set controller.ingressClassResource.default=true \
    --set controller.service.type=NodePort \
    --set controller.service.nodePorts.http=30080 \
    --set controller.service.nodePorts.https=30443

helm upgrade -i cert-manager jetstack/cert-manager \
    --namespace cert-manager \
    --create-namespace \
    --set installCRDs=true

kubectl create namespace argocd
helm upgrade -i argo-cd oci://registry-1.docker.io/bitnamicharts/argo-cd \
    --namespace argo-cd \
    --create-namespace \
    --set global.storageClass=local-storage

kubectl apply -f - <<EOF
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: local-storage
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
provisioner: kubernetes.io/no-provisioner
volumeBindingMode: WaitForFirstConsumer
allowVolumeExpansion: true
EOF

kubectl apply -f - <<EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: local-volume-3
spec:
  capacity:
    storage: 256Mi
  volumeMode: Filesystem
  accessModes:
    - ReadOnlyMany
  storageClassName: local-storage
  hostPath:
    path: /etc/volumes/volume3
EOF