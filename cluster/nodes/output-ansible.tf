# generate inventory file for Ansible
resource "local_file" "hosts_cfg" {
  content = templatefile("${path.root}/templates/hosts.tftpl",
    {
      master = aws_instance.master
    }
  )
  filename = "../ansible/hosts"
}

# generate file with master url
resource "local_file" "master_url"{
  content = "https://${aws_instance.master.public_ip}:9345"
  filename = "../ansible/master-url"
}