variable "aws_region" {
  type    = string
  default = "eu-central-1"
}

variable "availability_zones" {
  type        = list(string)
  default     = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
}

variable "project_name" {
  type    = string
  default = "project-1"
}

variable "cluster_name" {
  type    = string
  default = "k8s-cluster"
}

variable "s3_bucket_name" {
  type    = string
  default = "project-1-bucket"
}
