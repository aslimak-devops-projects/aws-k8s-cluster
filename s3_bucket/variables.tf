variable "bucket_name" {
  type        = string
  description = "The name of the Bucket"
  default     = "my-project-bucket"
}

variable "state_locks_name" {
  type        = string
  description = "The name of the state locks DynamoDB table"
  default     = "my-project-tf-state-locks"
}
