# AWS Kubernetes Cluster Provisioning with Terraform

## Overview
This repository contains Terraform files to provision a Kubernetes cluster in AWS. It consists of two main components:

1. **S3 Bucket:** Contains scripts to create an AWS S3 bucket to store Terraform state files and a DynamoDB table for state locks.

2. **Cluster:** Contains scripts for setting up a Kubernetes cluster on AWS. It handles the creation of essential infrastructure components like VPC, subnets, security groups, route tables, and autoscaling group for worker nodes. Additionally, it configures a load balancer and links it with worker nodes.

3. **Ansible:** Contains scripts to initialize the Kubernetes cluster using RKE2 and install a basic NGINX Ingress controller for health checks from the load balancer.

## Prerequisites
Before using these Terraform scripts, ensure you have the following:
- An AWS account with appropriate permissions to create resources.
- Terraform installed on your local machine.

## Usage

1. Clone this repository to your local machine:
    ```bash
    git clone https://github.com/aslimak-devops-projects/aws-k8s-cluster.git
    ```

2.  Set AWS credentials as environment variables, you can do so by exporting the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` variables in your terminal:
	```bash
	export AWS_ACCESS_KEY_ID=your_access_key_id
	export AWS_SECRET_ACCESS_KEY=your_secret_access_key
	export AWS_REGION=your_aws_region
	```

3. Navigate to the `s3_bucket` directory and modify the `terraform.tfvars` file for your requirements. Initialize Terraform:
    ```
    terraform init
    ```

4. Review the planned infrastructure changes and apply to create the S3 bucket:
    ```
    terraform plan
    terraform apply
    ```

5. After the S3 bucket setup is complete, navigate to the `cluster` directory and modify the `terraform.tfvars` file requirements to configure your requirements. Initialize Terraform:
    ```
    terraform init
    ```

6. Review the planned infrastructure changes and apply to create the Kubernetes cluster:
    ```
    terraform plan
    terraform apply
    ```

7. Fill in the addresses of worker nodes in the hosts file and then run:
    ```
    ansible-playbook setup-k8s-master.yaml
    ansible-playbook setup-k8s-workers.yaml
    ```

8. Once Terraform finishes provisioning, you'll have a fully functional Kubernetes cluster on AWS. Your Terraform state files will be securely stored in the S3 bucket.

## Configuration
Both the `cluster` and `s3_bucket` directories contain a `variables.tf` file with variables that you can customize according to your requirements. Ensure to modify the variables in both directories as needed. Also, remember to edit the `tfstate.tf` file accordingly, as it is not updated automatically from the variables file.

## License
This project is licensed under the [MIT License](LICENSE).